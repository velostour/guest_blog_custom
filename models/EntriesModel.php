<?php

/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 13.06.2016
 * Time: 14:44
 */
class EntriesModel extends ModelBase
{

    
    /*Моя серьезная ошибка. Я напрочь забыл про синглтон */ 
    protected static $instance;
    public static function getInstance() {
        return (self::$instance === null) ? self::$instance = new self(__CLASS__) : self::$instance;
    }

    /**
     * Выборка записей блога для главной
     * @param int $limit
     * @return mixed
     */
    public function getListEntries($limit=10 )
    {
        $entries = $this->dbase->queryRows("SELECT 
                                          entries.id AS entries_id,
                                            authors.name AS author_name, 
                                            authors.mail AS authors_mail,
                                            entry.text AS entry_text, 
                                            entry.title AS entry_title,
                                            entries.created_at AS entry_created_at
                                         FROM entries
                                         LEFT JOIN entry ON entries.entry_id=entry.id
                                         LEFT JOIN authors ON entries.author=authors.id 
                                         ORDER BY entry_created_at DESC 
                                         LIMIT ".$limit."
                                         ");

       return $entries;
    }


    /**
     * Выборка популярных по комментариям записей блога для главной в слайдер
     * @param int $limit
     * @return mixed
     */
    public function getListPopular( $limit=5 )
    {
        $popular = $this->dbase->queryRows("SELECT DISTINCT comments.entries AS entries_id, entry.text AS text, entry.title AS title, 
                                                   ( SELECT COUNT(comments.entries) FROM comments WHERE entries = entries_id) AS count_comments
                                         FROM entries
                                         LEFT JOIN comments ON entries.id=comments.entries
                                         LEFT JOIN entry ON entries.entry_id=entry.id
                                         ORDER BY count_comments DESC 
                                         LIMIT ".$limit."
                                         ");

        return $popular;
    }

    /**
     * Получение "тела" еденичной записи блогя для индивидуального просмотра. Далее по его данным
     * подтягиваются комментарии в отдельную сущность
     * @param null $id
     * @return mixed
     */
    public function getEntry($id=null)
    {
        $entries = $this->dbase->queryRow("SELECT entry.text, entry.title, authors.mail, authors.name,
                                                entries.id AS entries_id, 
                                                entry.id AS entry_id, 
                                                entries.created_at AS entry_created_at
                                         FROM entries
                                         LEFT JOIN entry ON entries.entry_id=entry.id
                                         LEFT JOIN authors ON entries.author=authors.id
                                         WHERE entries.id = ".$id."
                                         ");
        return $entries;
    }

    /**
     * Выборка всех комментариев по конкретной записи блога по ID
     * @param null $entries
     * @return mixed
     */
    public function getCommentsById($entries=null)
    {
        $comments = $this->dbase->queryRows("SELECT 
                                                  authors.name AS name, 
                                                  authors.mail AS mail, 
                                                  comments.created_at AS created_at, 
                                                  comment.text AS text, 
                                                  comments.id AS comments_id, 
                                                  comment.id AS comment_id
                                         FROM comments
                                         LEFT JOIN comment ON comment.id=comments.comment_id
                                         LEFT JOIN authors ON comments.author=authors.id
                                         WHERE entries = ".$entries."
                                         ORDER BY comments_id DESC 
                                         ");

        return $comments;
    }

    /**
     * Вставка сущности записи блога
     * Нужно было проводить это через транзакции, чтобы ы БД не накапливался безсвязный мусор в случае ошибок.
     * Даже при условии что именно в этом проекте ошибки записи я стралася минимизировать.
     * @param $formdata
     * @return mixed
     */
    public function addEntries($formdata)
    {
        $idAuthor = $this->addAuthor($formdata['AuthorForm']);
        $identry = $this->addEntry($formdata['EntryForm']);
        $entriesId = $this->dbase->insert('entries', [
            'author' => $idAuthor,
            'entry_id' => $identry,
            'created_at' => time(),
            'updated_at' => time()
        ]);
        
        return $entriesId;
    }

    /**
     * Вставка сущности комментария
     * @param $formdata
     * @return mixed
     * @throws Exception
     */
    public function addCommentRow($formdata)
    {
        // только при наличии существующей сущности записи блога мы можем пристегнуть к ней сущность комментария
        if ( $entries = $this->getEntry($this->cleanFull($formdata['CommentForm']['entries'])) ) {
            $idAuthor = $this->addAuthor($formdata['AuthorForm']);
            $idComment = $this->addComment($formdata['CommentForm']);
            $commentRowId = $this->dbase->insert('comments', [
                'author' => $idAuthor,
                'entries' => (int)$entries['entries_id'],
            'comment_id' => $idComment,
            'created_at' => time(),
            'updated_at' => time()
        ]);
            return $commentRowId;
        } else {
            throw new Exception('Нет такой записи или запись запрещена');
        }
    }

    /**
     * Получаем ID существующего по имейлу автора или новоинсерченого
     * @param $data
     * @return int
     */
    protected function addAuthor($data)
    {
        $mail = $this->cleanFull($data['mail']);
        $name = $this->cleanFull($data['name']);
        $authorId = $this->dbase->queryRow('SELECT id FROM authors WHERE mail=:mail', [':mail' => $mail ]);
        if ( $authorId )
            return (int)$authorId['id'];

        $authorId = $this->dbase->insert('authors', [
            'mail' => $mail,
            'name' => $name,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        return (int)$authorId;
    }

    /**
     * Инсертим содержание записи блога с возвратом ID для создания сущности записи блога
     * @param $data
     * @return int
     */
    protected function addEntry($data)
    {
        $text = $this->cleanFull($data['text']);
        $title = $this->cleanFull($data['title']);
        $entryId = $this->dbase->insert('entry', [
            'text' => $text,
            'title' => $title
        ]);

        return (int)$entryId;
    }

    /**
     * Инсертим содержание комментария с возвратом ID для создания сущности комментария
     * @param $data
     * @return int
     */
    protected function addComment($data)
    {
        $text = $this->cleanFull($data['text']);
        $commentId = $this->dbase->insert('comment', [
            'text' => $text,
        ]);

        return (int)$commentId;
    }

    /**
     * Стандартная очистка пост данных
     * Можно расширять влоть до проверок по регуляркам
     * @param $data
     * @return string
     */
    private function cleanFull($data)
    {
        $input_text = trim($data);
        $input_text = strip_tags($input_text);
        $input_text = htmlspecialchars($input_text, ENT_QUOTES);
        $input_text = addslashes($input_text);
        return $input_text;
    }
    
    /**
     * Методы проверки каждой исходящей формы.
     * Минимальная реализация проверки на существование.
     * В зависимости от типа формы и набора затребуемых правил проверок можно расширять до нужных масштабов.
     * Хотя в таком виде можно было не размножать и реализовать через анонимку внутри checkForm()
     * Исключительно для внутреннего использования.
     * @param $formData
     * @return bool
     */
    private function checkCommentForm($formData)
    {
        foreach ( $formData as $name => $value ) {
            if ( empty( $formData[$name] ) ) {
                SessionBase::setFlash( 'Поле '.$name.' необходимо заполнить' , 'danger');
                return false;
            }
        }
    }

    private function checkAuthorForm($formData)
    {
        foreach ( $formData as $name => $value ) {
            if ( empty($formData[$name]) ) {
                SessionBase::setFlash( 'Поле '.$name.' необходимо заполнить' , 'danger');
                return false;
            }
        }
    }

    private function checkEntryForm($formData)
    {
        foreach ( $formData as $name => $value ) {
            if ( empty($formData[$name]) ) {
                SessionBase::setFlash( 'Поле '.$name.' необходимо заполнить' , 'danger');
                return false;
            }
        }
    }


    /**
     * Если хоть одно правило не пройдет проверку прерываем с фальсом и показываем флешку с проблемой.
     * @param $data
     * @return bool
     */
    public function checkForm($data)
    {
        foreach ( $data as $formName => $formData) {
            if ( isset($data[$formName]) ) {
                $checkFormName = 'check'.$formName;
                if ( $this->$checkFormName($formData) === false  ) return false;
            }
        }

        return true;
    }


}