<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 11.06.2016
 * Time: 14:03
 */

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
$sitePath = realpath( dirname( __FILE__ )  );
defined('ROOT') or define('ROOT', $sitePath);
defined('SERVER_ROOT') or define('SERVER_ROOT', ROOT . DS . '..' );
defined('VIEW_PATH') or define('VIEW_PATH', SERVER_ROOT . DS . 'views' );

require_once( SERVER_ROOT . DS . 'core' . DS . 'autoload.php');

AppBase::run($_SERVER['REQUEST_URI']);