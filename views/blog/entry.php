<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 12.06.2016
 * Time: 18:06
 */

?>
<div class="entries-view">
    <h1><?= $entry['title'] ?></h1>
    <p> Некий <b><?= $entry['name'] ?></b> написал это
        <?= date('d.m.Y' , $entry['entry_created_at'])   ?>
        в <?= date('H:i:s' , $entry['entry_created_at'])  ?>
    </p>
    <p>
        <?= $entry['text'] ?>
    </p>
</div>


<!--ФОРМА КОММЕНТАРИЕВ Честно содрал из предыдущей работы -->

<div class="row" style="
    padding-top: 40px;
        margin-top: 25px;
    border-top: 3px solid #ddd;
">
    <div class="col-lg-10 col-lg-offset-1">
        <p>Есть что сказать? Не стесняйтесь ;) </p>
        <div class=" entries-form">
            <form id="w1" action="/<?= AppBase::getRouter()->getUri() ?>" method="post">
                <div class="form-group field-commentform-entries">
                    <input type="hidden" id="commentform-entries" class="form-control" name="CommentForm[entries]" value="<?= $entry['entries_id'] ?>">
                    <div class="help-block"></div>
                </div>    <div class="row">
                    <div class="col-md-6 form-group field-authorform-name">
                        <label class="control-label" for="authorform-name">Ваше имя </label>
                        <input type="text" id="authorform-name" class="form-control" name="AuthorForm[name]" value="<?= isset($_POST['AuthorForm']) ? $_POST['AuthorForm']['name'] : '' ?>">

                        <div class="help-block"></div>
                    </div>        <div class="col-md-6 form-group field-authorform-mail required">
                        <label class="control-label" for="authorform-mail">Email </label>
                        <input type="text" id="authorform-mail" class="form-control" name="AuthorForm[mail]" value="<?= isset($_POST['AuthorForm']) ? $_POST['AuthorForm']['mail'] : '' ?>">

                        <div class="help-block"></div>
                    </div>    </div>


                <div class="form-group field-commentform-text required">
                    <label class="control-label" for="commentform-text">Ваши идеи превосходны </label>
                    <textarea id="commentform-text" class="form-control" name="CommentForm[text]" maxlength="1000" rows="7" cols="100"><?= isset($_POST['CommentForm']) ? $_POST['CommentForm']['text'] : false ?></textarea>

                    <div class="help-block"></div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить свой бесценный комментарий</button>    </div>

            </form>
        </div>
    </div>
</div>

<!--    Рендерим комментарии к записи-->

<div class="row" style="
    padding-top: 30px;
        margin-top: 25px;
    border-top: 3px solid #ddd;
">

<? if ( !empty($comments)  ) { ?>
    <? foreach ($comments as $num => $comment) { ?>
        <div class="col-md-12">
            <div class="testimonials">
                <div class="active item">

                    <div class="col-md-3">
                        <div class="carousel-info">
                            <img alt="" src="<?= HelpersBase::avatar($comment['mail']) ?>" class="pull-left">
                            <div class="pull-left">
                                <span class="testimonials-name"><?= $comment['name'] ?></span>
                                <span class="testimonials-post"><?= date('d.m.Y' , $comment['created_at'])   ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <blockquote><p><?= $comment['text'] ?></p></blockquote>
                    </div>
                </div>
            </div>
        </div>
    <? }

} else { ?>
    <div class="col-md-12">
        <p>No comments</p>
    </div>
<? } ?>
</div>