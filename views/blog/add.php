<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 13.06.2016
 * Time: 23:30
 */
?>


<div class="entries-create">

    <h1>Делитесь опытом. Ведь так зародился Open source</h1>


    <div class="entries-form">

        <form id="entry" action="/blog/add" method="post">
            <div class="row">
                <div class="col-md-6 form-group field-authorform-name">
                    <label class="control-label" for="authorform-name">Ваше имя </label>
                    <input type="text" id="authorform-name" class="form-control" name="AuthorForm[name]" value="<?= isset($_POST['AuthorForm']) ? $_POST['AuthorForm']['name'] : '' ?>">

                    <div class="help-block"></div>
                </div>    <div class="col-md-6 form-group field-authorform-mail required">
                    <label class="control-label" for="authorform-mail">Email </label>
                    <input type="text" id="authorform-mail" class="form-control" name="AuthorForm[mail]" value="<?= isset($_POST['AuthorForm']) ? $_POST['AuthorForm']['mail'] : '' ?>">

                    <div class="help-block"></div>
                </div>    </div>

            <div class="form-group field-entryform-title required">
                <label class="control-label" for="entryform-title">Заголовок </label>
                <input type="text" id="entryform-title" class="form-control" name="EntryForm[title]" value="<?= isset($_POST['EntryForm']) ? $_POST['EntryForm']['title'] : '' ?>">

                <div class="help-block"></div>
            </div>    <div class="form-group field-entryform-text required">
                <label class="control-label" for="entryform-text">Содержание </label>
                <textarea id="entryform-text" class="form-control" name="EntryForm[text]" maxlength="1000" rows="7" cols="100"><?= isset($_POST['EntryForm']) ? $_POST['EntryForm']['text'] : false ?></textarea>

                <div class="help-block"></div>
            </div>
            <div class="captcha field-entryform-recaptcha required">
                
                <div class="help-block"></div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Можно бесплатно что-то написать</button>    </div>

        </form>
    </div>

</div>

