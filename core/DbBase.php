<?php

/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 12.06.2016
 * Time: 21:43
 * Старая добрая библиотека. Кочует со мной чуть ли не с первого места работы.
 */
class DbBase
{
    /**
     * @var PDO
     */
    protected $connection;

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * DbBase constructor.
     * @param $dsn
     * @param $username
     * @param $passwd
     */
    public function __construct($dsn, $username, $passwd )
    {
        try {
            $this->connection = new PDO( $dsn, $username, $passwd );
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Подключение не удалось: ' . $e->getMessage());
        }

    }

    /**
     * @param $table
     * @param $fields
     * @param null $insertParams
     * @return null|string
     */
    public function insert($table, $fields=[], $insertParams = null) {
        try {
            $result = null;
            $names = '';
            $vals = '';
            foreach ($fields as $name => $val) {
                if (isset($names[0])) {
                    $names .= ', ';
                    $vals .= ', ';
                }
                $names .= $name;
                $vals .= ':' . $name;
            }
            $ignore = isset($insertParams['ignore']) && $insertParams['ignore']? 'IGNORE': '';
            $sql = "INSERT $ignore INTO " . $table . ' (' . $names . ') VALUES (' . $vals . ')';
            $resourse = $this->connection->prepare($sql);
            foreach ($fields as $name => $val) {
                $resourse->bindValue(':' . $name, $val);
            }
            if ($resourse->execute()) {
                $result = $this->connection->lastInsertId(null);
            }
            return $result;
        } catch(Exception $e) {
            $this->report($e);
        }
    }

    // Returns true/false
    /**
     * @param $table
     * @param $fields
     * @param $where
     * @param null $params
     * @return bool
     */
    public function update($table, $fields=[], $where, $params = null) {
        try {
            $sql = 'UPDATE ' . $table . ' SET ';
            $first = true;
            foreach (array_keys($fields) as $name) {
                if (!$first) {
                    $first = false;
                    $sql .= ', ';
                }
                $first = false;
                $sql .= $name . ' = :_' . $name;
            }
            if (!is_array($params)) {
                $params = array();
            }
            $sql .= ' WHERE ' . $where;
            $resourse = $this->connection->prepare($sql);
            foreach ($fields as $name => $val) {
                $params[':_' . $name] = $val;
            }
            $result = $resourse->execute($params);
            return $result;
        } catch(Exception $e) {
            $this->report($e);
        }
    }

    /**
     * @param $query
     * @param null $params
     * @return null|string
     */
    public function queryValue($query, $params = null) {
        try {
            $result = null;
            $statement = $this->connection->prepare($query);
            if ($statement->execute($params)) {
                $result = $statement->fetchColumn();
                $statement->closeCursor();
            }
            return $result;
        } catch(Exception $e) {
            $this->report($e);
        }
    }

    /**
     * @param $query
     * @param null $params
     * @return array|null
     */
    public function queryValues($query, $params = null) {
        try {
            $result = null;
            $statement = $this->connection->prepare($query);
            if ($statement->execute($params)) {
                $result = array();
                while ($row = $statement->fetch(PDO::FETCH_NUM)) {
                    $result[] = $row[0];
                }
            }
            return $result;
        } catch(Exception $e) {
            $this->report($e);
        }
    }

    /**
     * @param $query
     * @param null $params
     * @param int $fetchStyle
     * @param null $classname
     * @return array|mixed|null
     */
    public function queryRow($query, $params = null, $fetchStyle = PDO::FETCH_ASSOC, $classname = null) {
        $rows = $this->queryRowOrRows(true, $query, $params, $fetchStyle, $classname);
        return $rows;
    }

    /**
     * @param $query
     * @param null $params
     * @param int $fetchStyle
     * @param null $classname
     * @return array|mixed|null
     */
    public function queryRows($query, $params = null, $fetchStyle = PDO::FETCH_ASSOC, $classname = null) {
        $rows = $this->queryRowOrRows(false, $query, $params, $fetchStyle, $classname);
        return $rows;
    }

    /**
     * @param $singleRow
     * @param $query
     * @param null $params
     * @param int $fetchStyle
     * @param null $classname
     * @return array|mixed|null
     */
    private function queryRowOrRows($singleRow, $query, $params = null, $fetchStyle = PDO::FETCH_ASSOC, $classname = null) {
        try {
            $result = null;
            $statement = $this->connection->prepare($query);
            if($classname) {
                $statement->setFetchMode($fetchStyle, $classname);
            } else {
                $statement->setFetchMode($fetchStyle);
            }
            if ($statement->execute($params)) {
                $result = $singleRow? $statement->fetch(): $statement->fetchAll();
                $statement->closeCursor();
            }
            return $result;
        } catch(Exception $e) {
            $this->report($e);
        }
    }

    /**
     * @param $str
     * @return string
     */
    public function quote($str) {
        return $this->connection->quote($str);
    }

    /**
     * @param $arr
     * @return array
     */
    public function quoteArray($arr) {
        $result = array();
        foreach ($arr as $val) {
            $result[] = $this->connection->quote($val);
        }
        return $result;
    }

    /**
     * @param $arr
     * @return string
     */
    public function quoteImplodeArray($arr) {
        return implode(',', $this->quoteArray($arr));
    }

    // returns true/false
    /**
     * @param $query
     * @param null $params
     * @return bool
     */
    public function sql($query, $params = null) {
        try {
            $result = null;
            $statement = $this->connection->prepare($query);
            return $statement->execute($params);
        } catch(Exception $e) {
            $this->report($e);
        }
    }

    /**
     * @param $e
     */
    private function report($e) {
        throw $e;
    }

}