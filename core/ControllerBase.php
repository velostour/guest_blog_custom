<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 11.06.2016
 * Time: 18:20
 */

class ControllerBase
{
    /**
     * @var array
     */
    protected $data;
    /**
     * @var
     */
    protected $model;
    /**
     * @var
     */
    protected $params;
    /**
     * @var
     */
    protected $view;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * ControllerBase constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
        $this->params = \AppBase::getRouter()->getParams();
        $this->view = new ViewBase();
    }

    public function redirect($url)
    {
        header('Location: '.$url);
        exit;
    }

}