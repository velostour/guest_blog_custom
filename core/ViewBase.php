<?php

/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 12.06.2016
 * Time: 17:39
 */
class ViewBase
{
    protected $data;
    protected $path;
    protected $layout;

    public function __construct( $data = [] , $path = null )
    {
        if(!$path) {
            $path = self::getViewPath();
        }
        if ( !file_exists($path) ) {
            throw new Exception('Нет такого шаблона '. $path );
        }
        $this->data = $data;
        $this->path = $path;
    }

    protected static function getViewPath()
    {
        $router = AppBase::getRouter();
        if ( !$router ) {
            throw new Exception('Роутер не найден. Нужон Одмин.');
        }
        $controller_directory = str_replace('controller','', strtolower($router->getController()));
        $template_file = str_replace('action','', strtolower($router->getAction())) . '.php';
        return VIEW_PATH . DS . $controller_directory . DS . $template_file;
    }

    
    // получить отрендеренный шаблон с параметрами $params
    public function fetchPartial($template, $params = [] ){
        extract($params);
        ob_start();
        include( $this->path );
        return ob_get_clean();
    }

    // получить отрендеренный шаблон с параметрами $params
    protected function fetchLayout($template, $params = [] ){
        extract($params);
        ob_start();
        include( $template );
        return ob_get_clean();
    }

    // вывести отрендеренный шаблон с параметрами $params
    public function renderPartial($template, $params = [] ){
        echo $this->fetchPartial($template, $params);
    }

    // получить отрендеренный в переменную $content layout-а
    // шаблон с параметрами $params
    public function fetch( $template, $params = [] ){
        $content = $this->fetchPartial($template, $params);
        return $this->fetchLayout( $this->getLayout() , [ 'content' => $content ]);
    }

    // вывести отрендеренный в переменную $content layout-а
    // шаблон с параметрами $params
    public function render($template, $params = [] ){
        echo $this->fetch($template, $params);
    }

    /**
     * @return array
     */
    public function getLayout()
    {
        $layout = AppBase::getRouter()->getController();
        $layout = str_replace('controller','',strtolower($layout));
        $layout_source = VIEW_PATH . DS . 'layouts' . DS . $layout . '.php';
        return$layout_source;
    }






}