<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 11.06.2016
 * Time: 14:58
 */

class RouterBase {

    protected $uri;
    protected $controller;
    protected $action;
    protected $params;
    protected $route;

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }
    
    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    public function __construct($uri)
    {
        $this->uri = urldecode( trim( $uri , '/') );

        // выставляем дефолты. Перестраховка и предусмотрительность
        $routes = ConfigBase::get('routes');
        $this->route = ConfigBase::get('default_route');
        $this->controller = self::controllerName(ConfigBase::get('default_controller'));
        $this->action = self::actionName(ConfigBase::get('default_action'));

        $uri_block = explode('?',$this->uri);
        $main_path = $uri_block[0]; // обрезаем весь ненужный мусор оставляя чистый юри. Чтоб не баловались зловреды
        $uri_elements = explode('/', $main_path);

        if ( count($uri_elements) ) {
            // сперва перебираем правила маршрутизатора, и не найдя совпадения разочаровываемся и ищем контроллер с таким именем напрямую
            // Так или иначе если первый параметр есть - делаем его именем класса контроллера.
            if ( in_array( strtolower(current($uri_elements)) , array_keys( $routes )) ) {
                $this->route = strtolower(current($uri_elements));
                $this->controller = self::controllerName($routes[$this->route]);
                array_shift($uri_elements);
            } else {
                if ( current($uri_elements) ) {
                    $this->controller = self::controllerName(strtolower(current($uri_elements)));
                    array_shift($uri_elements);
                }
            }
            // далее дело за малым получить екшннейм
            if ( current($uri_elements) ) {
                $this->action = self::actionName(current($uri_elements));
                array_shift($uri_elements);
            }

            // А объедки с барского стола идут в параметры запроса. Простота больше похожа на codeigniter/
            $this->params = $uri_elements;
        }

    }

    protected static function actionName($name)
    {
        return 'action'.ucfirst($name);
    }

    protected static function controllerName($name)
    {
        return ucfirst( $name ) .'Controller';
    }

}