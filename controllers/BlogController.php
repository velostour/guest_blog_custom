<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 11.06.2016
 * Time: 17:39
 */

class BlogController extends ControllerBase
{

    /**
     * @var EntriesModel
     */
    protected $model;

    /**
     * BlogController constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct($data);
        return $this->model = EntriesModel::getInstance();
    }

    /**
     * Рендер главной с маджонгом и гейшами
     */
    public function actionIndex()
    {
        $this->getParams();
        $model = $this->model;
        $entries = $model->getListEntries();
        $popular = $model->getListPopular();
        $data = [
            'model' => $model,
            'entries' => $entries,
            'popular' => $popular,
        ];

        return $this->view->render('index', $data );
    }

    /**
     * Рендерим сущность записи по ID из параметров роута
     * Так же принимаем пост из формы добавления коммента к текущей записи блога и обновляемся здесь же при успешном комменте.
     */
    public function actionEntry()
    {
        $model = $this->model;
        $entries_id = $this->getParams()[0];
        $entry = $model->getEntry($entries_id);
        if ( !$entry ) throw new ErrorException('Ошибка 404 Записи с номером '.$entries_id.' не существует.');
        if ( !empty($_POST) && $model->checkForm($_POST) )  {
            $entriesId = $model->addCommentRow($_POST);
            if ($entriesId) {
                return $this->redirect(DS.'blog'.DS.'entry'.DS.$entries_id);
            }
        }
        $comments = $model->getCommentsById($entries_id);
        $data = [
            'model' => $model,
            'entry' => $entry,
            'comments' => $comments,
        ];

        return $this->view->render('entry', $data );
    }

    /**
     * Добавление новой записи блога
     */
    public function actionAdd()
    {
        $model = $this->model;
        $entries = $model->getListEntries();
        if ( !empty($_POST) && $model->checkForm($_POST) ) {
            $entriesId = $model->addEntries($_POST);
            if ($entriesId) {
                return $this->redirect(DS.'blog'.DS.'entry'.DS.$entriesId);
            }
        }
        $data = [
            'model' => $model,
            'entries' => $entries,
        ];

        return $this->view->render('index', $data );
    }
    

}


